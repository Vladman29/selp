package com.example.s1512322.songle;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;

public class AchievementsActivity extends AppCompatActivity {
    public SharedPreferences sharedPreferences;
    public String text;
    public String[] songs;
    public String[] links;
    public final String TAG = "Achievements Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_achievements);

        getSongs();
        getLinks();
        writeAchievements();
    }

    private void writeAchievements(){

        Log.d(TAG,"writeAchievements");
        text = "";
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        String user = sharedPreferences.getString("user_email","");
        String achievements = sharedPreferences.getString(user+"_achievements","");

        TextView achText = findViewById(R.id.achText);


        if(achievements.equals(""))
        {
            achText.setText("No songs guessed yet! :(");
        }
        else{
            text = "";
            add_to_text("EASIEST",achievements,5);
            add_to_text("EASIER",achievements,4);
            add_to_text("MEDIUM",achievements,3);
            add_to_text("HARD", achievements,2);
            add_to_text("VERY HARD", achievements, 1);
            achText.setMovementMethod(LinkMovementMethod.getInstance());
            achText.setText(Html.fromHtml(text));
        }
    }

    private void add_to_text(String diff, String achievements, int num){
        Log.d(TAG, "add_to_text");
        Log.d(TAG, "The difficulty is " + diff);
        Log.d(TAG, "The achievements string is " + achievements);
        Log.d(TAG, "The number searched in the string is " + num);
        text = text + "<br>" + diff + ":<br>";
        int len = achievements.length();
        int counter = 1;
        for(int i=0; i<len; i++){
            if(achievements.charAt(i) == String.valueOf(num).charAt(0)){
                text = text + String.valueOf(counter) + ". <font color='#3f51b5'> <a href=\"" +
                        links[i] + "\">" + songs[i] + "</a> </font> <br>";
                counter++;
            }
        }
    }

    private void getSongs(){
        String raw_data = getIntent().getExtras().getString("songs");
        Log.d(TAG, "The songs are: " + raw_data);
        songs = raw_data.split(";");
    }

    private void getLinks(){
        String raw_data = getIntent().getExtras().getString("links");
        Log.d(TAG, "The links are: " + raw_data);
        links = raw_data.split(";");
    }
}
