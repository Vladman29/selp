package com.example.s1512322.songle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class StartActivity extends AppCompatActivity {

    // Start Activity is the first activity of Songle. Its main purpose is to gather user_name and
    // user_email from the user. Once gathered, these two fields will be stored into shared
    // preferences and used by the app later. If the user chooses FB login, the info will be taken
    // from their FB account. Otherwise the name they choose will be both their email and name.

    private CallbackManager callbackManager;
    private LoginButton fbLoginButton;
    public static String TAG = "Start_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");

        setContentView(R.layout.activity_start);

        // Code for the basic login button and the how_to_play button
        Button tutorialButton = (Button) findViewById(R.id.tutorialButton);
        Button loginButton = (Button) findViewById(R.id.loginButton);

        tutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToInstructions();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginNoFb();
            }
        });

        // Facebook login stuff
        callbackManager = CallbackManager.Factory.create();
        fbLoginButton = findViewById(R.id.login_button);
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile","email"));

        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG,"FB Login Successful");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback(){
                            @Override
                            public void onCompleted(JSONObject obj, GraphResponse response ) {
                                try {
                                    // Get user email and name and store them in shared preferences.
                                    String email = obj.getString("email");
                                    String name = obj.getString("name");
                                    SharedPreferences sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("user_email", email);
                                    editor.putString("user_name", name);
                                    editor.commit();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d(TAG,"Error");;
                                }


                            }
                        }
                );

                Bundle parameters = new Bundle();
                parameters.putString("fields", "name,email");
                request.setParameters(parameters);
                request.executeAsync();


                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }

                    @Override
                    public void onCancel() {

                        System.out.println("Login cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("Exception");
                    }
                });

    }

    // function to go to the Instructions page
    private void goToInstructions(){
        Log.d(TAG,"goToInstructions");
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

    private void loginNoFb(){
        // Use the name given by user as both email and name and put those into shared preferences.
        Log.d(TAG, "loginNoFb");
        EditText user = (EditText) findViewById(R.id.user);
        String username = user.getText().toString();
        SharedPreferences sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putString("user_email", username);
        e.putString("user_name", username);
        e.apply();
        Intent intent = new Intent( this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"onActivityResult");
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
