package com.example.s1512322.songle;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by s1512322 on 03/12/17.
 */

public class SongParser {
    // We don't use namespaces.
    private static final String ns = null;

    public List parse(InputStream in) throws XmlPullParserException, IOException{
        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException{
        List songs = new ArrayList();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            String tag = parser.getName();
            if( tag.equals("Song")){
                songs.add(readSong(parser));
            }
            else{
                skip(parser);
            }
        }

        return songs;
    }

    public class Song{
        private final String number;
        private final String artist;
        private final String title;
        private final String link;

        Song(String number, String artist, String title, String link){
            this.number = number;
            this.artist = artist;
            this.link = link;
            this.title = title;
        }

    public String getNumber() { return  this.number;}

    public String getArtist() { return  this.artist;}

    public String getTitle() { return  this.title;}

    public String getLink() { return  this.link;}

    public String toString(){
            return getNumber() + ";" + getArtist() + ";" + getTitle() + ";" + getLink() + ";";
        }
    }

    private Song readSong(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, "Song");

        String number = null;
        String artist = null;
        String link = null;
        String title = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String tag = parser.getName();
            if (tag.equals("Number")){
                number = readNumber(parser);
            } else if (tag.equals("Artist")){
                artist = readArtist(parser);
            } else if (tag.equals("Title")){
                title = readTitle(parser);
            } else if (tag.equals("Link")){
                link = readLink(parser);
            } else {
                skip(parser);
            }
        }

        return new Song(number, artist, title, link);
    }

    private String readNumber(XmlPullParser parser) throws IOException, XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "Number");
        String number = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Number");
        return number;
    }

    private String readArtist(XmlPullParser parser) throws IOException, XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "Artist");
        String artist = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Artist");
        return artist;
    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "Title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Title");
        return title;
    }

    private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "Link");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "Link");
        return title;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String text = "";
        if (parser.next() == XmlPullParser.TEXT) {
            text = parser.getText();
            parser.nextTag();
        }
        return text;
    }

    private void skip(XmlPullParser parser) throws IOException, XmlPullParserException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}



